Name:             android-udev-rules
Version:          20210501
Release:          1%{?dist}
Summary:          Android udev rules list aimed to be the most comprehensive on the net
License:          GPLv3+
URL:              https://github.com/M0Rf30/android-udev-rules
Source0:          https://github.com/M0Rf30/%{name}/archive/%{version}/%{name}-%{version}.tar.gz
BuildArch:        noarch
BuildRequires:    systemd-rpm-macros
Requires(pre):    shadow-utils
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd

%{?sysusers_requires_compat}

%description
These rules refer to 'Run Apps on a Hardware Device - Android Studio'
and include many suggestions from the Archlinux and Github Communities.

%prep
%autosetup -p1

%install
install -D -m 0644 -t %{buildroot}%{_sysusersdir} android-udev.conf
install -D -m 0644 -t %{buildroot}%{_udevrulesdir} 51-android.rules

%pre
%sysusers_create_compat android-udev.conf

%files
%license LICENSE
%doc README.md
%{_sysusersdir}/android-udev.conf
%{_udevrulesdir}/51-android.rules

%changelog
* Fri Aug 28 2020 Brandon Nielsen <nielsenb@jetfuse.net> - 20200613-1
- Initial port of OpenSUSE specfile
